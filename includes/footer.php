<?php
	$TO_ROOT =  str_repeat('../', preg_match_all('~[\\/]~', preg_replace('~SingpointHomepage[\\/]~', '', $_SERVER['SCRIPT_NAME'])) - 1);// Unterverzeichnis 'SingpointHomepage' bei Arbeit im localhost ignorieren
?><div id="footer" style="background:url(<?php echo $TO_ROOT;?>img/bg_balken.gif) center top no-repeat; padding-top:32px;">
		<table class="navi" cellpadding="10" cellspacing="0" style="width:100%; padding: 0 4px; border:0;">
			<tr valign="top">
				<td width="148" style="padding-bottom:1em;">
					<p>
						Singpoint</p>
					<a href="<?php echo $TO_ROOT;?>studios.html">
						Studios</a><br/>
					<a href="<?php echo $TO_ROOT;?>vstudio.html">
						V-Studio</a><br/>
					<a href="<?php echo $TO_ROOT;?>sing-box.html">
						Sing-Box</a><br/></td>
                <td width="148">
					<p>
						Kontakt</p>
					<a href="<?php echo $TO_ROOT;?>contact.html">
						Kontaktdaten</a><br/>
					<a href="<?php echo $TO_ROOT;?>feedback/index.php">
						Feedback</a><br/>
                    <a href="<?php echo $TO_ROOT;?>contact.html">
						Deine Idee</a><br/>
					<a href="<?php echo $TO_ROOT;?>contact.html">
						Newsletter</a><br/></td>
				<td width="148">
					<p>
						Unterhaltung</p>
					<p>
						<a href="<?php echo $TO_ROOT;?>Singpoint_TV.html">
							Singpoint TV<br /></a>
						<a href="<?php echo $TO_ROOT;?>news_network.html">
							News &amp; Network<br /></a>
						<a href="<?php echo $TO_ROOT;?>Spiele.html">
							Spiele</a><br/>
						<a href="<?php echo $TO_ROOT;?>Vocal_Coaching.html">
							Vocal-Coaching</a><br/>
						<a href="<?php echo $TO_ROOT;?>#"></a><br/></p></td>
				<td width="148">
					<p>
						B2B</p>
					<a href="<?php echo $TO_ROOT;?>Firmenkunden.html">
						Firmenkunden</a><br/>
					<a href="<?php echo $TO_ROOT;?>Partnerprogramme.html">
						Partnerprogramme</a><br/>
					<a class="navi" href="<?php echo $TO_ROOT;?>Sponsoring.html">
						Sponsoring</a><br/>
					<a href="<?php echo $TO_ROOT;?>Presse.html">
						Presse</a><br/></td>
				<td width="148">
					<p>
						Shopping </p>
					<p>
						<a href="<?php echo $TO_ROOT;?>playbacks_songtexte.html" target="_blank">
							Playbacks &amp; Lyrics</a>
						<a href="<?php echo $TO_ROOT;?>contact.html" style="padding-top:1em;">
							CD-Nachbestellung<br></a>
						<a href="<?php echo $TO_ROOT;?>gutschein/index.php">
							Gutscheine<br></a>
						<a href="<?php echo $TO_ROOT;?>sing_shop.html">
							Sing-Shop</a><br/></p></td>
				<td width="148">
					<p>
						Specials</p>
					<a href="<?php echo $TO_ROOT;?>club/">
						Club</a><br/>
					<a href="<?php echo $TO_ROOT;?>Angebot_des_Monats.html">
						Angebot des Monats</a><br/>
					<a href="<?php echo $TO_ROOT;?>club/casting_public.php" target="_blank">
						Casting</a><br /></td>
				<td width="62">
					<p>
						About</p>
					<p>
						<a href="<?php echo $TO_ROOT;?>UberSingpoint.html">
							Über SP</a><br/>
						<a href="<?php echo $TO_ROOT;?>faq.html">
							FAQ</a><br/>
						<a href="<?php echo $TO_ROOT;?>Impressum.html">
							Impressum</a><br />
						<a href="<?php echo $TO_ROOT;?>Jobs.html">
							Jobs</a><br/></p></td></tr></table>
		<div class="foot">
			Copyright &copy; Singpoint. All rights reserved.<br />
			Durch die Nutzung dieser Website akzeptieren Sie unsere Nutzungsbedingungen.</div></div>