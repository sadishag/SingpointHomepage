<?php
	$TO_ROOT =  str_repeat('../', preg_match_all('~[\\/]~', preg_replace('~SingpointHomepage[\\/]~', '', $_SERVER['SCRIPT_NAME'])) - 1);// Unterverzeichnis 'SingpointHomepage' bei Arbeit im localhost ignorieren
?>
	<div id="top">
		<div style="height: 105px; background:url(<?php echo $TO_ROOT;?>img/bg_balken.gif) center bottom no-repeat;">
			<div class="mainlogo" onClick="document.location.href='http://www.singpoint.de/';"></div>
			<div style="float: right; margin-top: 37px; margin-right: 10px; height: 22px; width: 500px">
				<div style="float:right;">
					<img alt="Deutschland Flagge" src="<?php echo $TO_ROOT;?>img/flag_de.gif" style="margin-right: 5px"/>
					<img alt="England Flagge" src="<?php echo $TO_ROOT;?>img/flag_en.gif" style="margin-right: 5px"/>
					<img alt="Frankreich Flagge" src="<?php echo $TO_ROOT;?>img/flag_fr.gif"/></div>
				<div style="float:right; padding-top: 4px; margin-right: 10px">
					<ul class="top_navigation">
						<li>
							<a href="<?php echo $TO_ROOT;?>UberSingpoint.html">
								Über Singpoint</a></li>
						<li id="menu_trigger1" onmouseover="menu_activ('menu_label1',true,'menu_sub1');" onmouseout="menu_activ('menu_label1',false,'menu_sub1');">
							<a id="menu_label1" href="#">
								Unser Angebot</a>
							<div class="menu_sub" id="menu_sub1" onMouseOver="menu_activ('menu_label1',true,null);">
								<img alt="Pfeil zu Tonstudio Angeboten" src="<?php echo $TO_ROOT;?>img/submenu_arrow.jpg"/>
								<div class="submenu_content">
									<h4>
										Unser Angebot</h4>
										<div class="submenu_col">
											<ul>
												<li>
													<a href="<?php echo $TO_ROOT;?>kindergeburtstag.html">
														Kindergeburtstag</a></li>
												<li>
													<a href="<?php echo $TO_ROOT;?>sing-point_event_info.html">
														Firmenfeiern</a></li>
												<li>
													<a href="<?php echo $TO_ROOT;?>junggesellenabschied.html">
														Junggesellenabschied</a></li>
												<li>
													<a href="<?php echo $TO_ROOT;?>junggesellinnenabschied.html">
														Junggesellinnenabschied</a></li>
												<li>
													<a href="<?php echo $TO_ROOT;?>weihnachtsfeier.html">
														Weihnachtsfeier</a></li>
												<li>
													<a style="white-space:nowrap;" href="<?php echo $TO_ROOT;?>baby-cd.html">
														CD für&rsquo;s eigene Baby erstellen</a></li></ul></div>
										<div class="submenu_col">
											<ul>
												<li>
													<a href="<?php echo $TO_ROOT;?>studios_song_info.html">
														Dein eigener Song</a></li>
												<li>
													<a href="<?php echo $TO_ROOT;?>studios_pro_info.html">
														Demo-CD aufnehmen</a></li>
												<li>
													<a href="<?php echo $TO_ROOT;?>studios_pro.html">
														Bandaufnahmen</a></li>
												<li>
													<a href="<?php echo $TO_ROOT;?>studios_story_info.html">
														Dein Hörbuch auf CD</a></li>
												<li>
													<a href="<?php echo $TO_ROOT;?>musikvideo_info.html">
														Musikvideo produzieren</a></li></ul></div>
										<div class="submenu_col" id="last_col">
											<ul>
												<li>
													<a href="<?php echo $TO_ROOT;?>gutschein/">
														Gutscheine</a></li>
												<li>
													<a href="<?php echo $TO_ROOT;?>playbacks_songtexte.html">
														Playbacks &amp; Lyrics</a></li>
												<li>
													<a href="<?php echo $TO_ROOT;?>vstudio.html">
														Online Tonstudio</a></li>
												<li>
													<a href="<?php echo $TO_ROOT;?>Vocal_Coaching.html">
														Vocal Coaching</a></li>
												<li>
													<a href="<?php echo $TO_ROOT;?>club/">
														Singpoint Club</a></li></ul></div></div></div></li>
						<li>
							<a href="<?php echo $TO_ROOT;?>gutschein/index.php">
								Gutscheine</a></li>
						<li>
							<a href="<?php echo $TO_ROOT;?>faq.html">
								FAQ</a></li>
						<li class="top_navigation_last">
							<a href="<?php echo $TO_ROOT;?>contact.html">Kontakt</a></li></ul></div></div></div></div>
