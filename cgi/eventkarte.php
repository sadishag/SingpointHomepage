<?php
	/* #########################################################################################
	   # Codeschnipsel zur Erzeugung des HTML für die Deutschlandkarte mit den Event-Städten  #
	   #--------------------------------------------------------------------------------------#
	   # zu übergebende Variable:                                                             #
	   # $einzelneEvents: String oder String-Array mit den Events, die als Hyperlinks bei den #
	   # jeweiligen angezeigt werden sollen. Sollen alle möglichen Events angezeigt werden,   #
	   # kann der übergebene Wert mit 'ALL' abgekürzt werden.                                 #
	   # $style_float: (optional) 'left' oder 'right' für die Ausrichtung der Landkarte       #
	   #--------------------------------------------------------------------------------------#
	   # mögliche Events:                                                                     #
	   # - kindergeburtstag                                                                   #
	   # - junggesellenabschied                                                               #
	   # - junggesellinnenabschied                                                            #
	   ######################################################################################## */
	
	if (!isset($einzelneEvents)){
		$einzelneEvents = 'ALL';
		}
	$style_float = (isset($style_float))
		? " float:$style_float;"
		: '';
	$_SERVER['SCRIPT_NAME'] = preg_replace('~SingpointHomepage[\\/]~', '', $_SERVER['SCRIPT_NAME']); // Unterverzeichnis 'SingpointHomepage' bei Arbeit im localhost ignorieren
	$TO_ROOT = str_repeat('../', preg_match_all('~[\\/]~', $_SERVER['SCRIPT_NAME']) - 1);
	$UNTERVERZEICHNIS = substr($_SERVER['SCRIPT_NAME'], 1, strpos($_SERVER['SCRIPT_NAME'], '/', 1));
	echo'			<!-- SINGPOINT LANDKARTE START -->' . PHP_EOL .
		'			<img class="singpointLandkarteJpeg" src="http://www.singpoint.de/img/Pictures/Singpoint_Landkarte.jpg" alt="Singpoint Landkarte mit Standorten" style="float:left;" />' . PHP_EOL .
		'			<div id="map" class="singpointLandkarteNeutral" style="width: 307px; height: 350px; position:relative;' . $style_float. '">';
	$database = 'webseite';
	require_once('Database.class');
	$database = new Database(HOST, USER, PW, DATABASE);
	$database->setTableName('eventstaedte');
	$staedte = $database->fetchAll();
	unset($database);
	foreach($staedte as $item){
		if ($item['Anzeige']){
			printf("\n\t\t\t\t<div class=\"textlabel\" id=\"karteTextlabel%1\$s\">%1\$s</div>",
				$item['Stadtname']);
			}
	}
	foreach ($staedte as $item){
		$html = sprintf("
				<div class=\"superlabel\" id=\"singpointLandkarte%s\">
					<div class=\"info\">
						<div style=\"overflow:hidden;\">
							<span class=\"headline\">%s</span>
							<p>",
					$item['Stadtname'],
					(file_exists($TO_ROOT . $item['convertierterStadtname'].'.html'))
						? "<a href=\"$TO_ROOT{$item['convertierterStadtname']}.html\">Singpoint {$item['Stadtname']}</a>"
						: "Singpoint {$item['Stadtname']}");
		$eventArray = ($einzelneEvents == 'ALL')
			? array('kindergeburtstag', 'junggesellenabschied', 'junggesellinnenabschied')
			: (array)$einzelneEvents;
		foreach ($eventArray as $event){
			$eventExists = file_exists($TO_ROOT . "$event/$event-" . strtolower($item['convertierterStadtname']) . '.html');
			$html .= preg_replace('~\.\./' . $UNTERVERZEICHNIS . '~', '', 
						sprintf("
							<a href=\"$TO_ROOT$event%s.html\">
								%s%s</a><br />",
						($eventExists)
							? "/$event-" . strtolower($item['convertierterStadtname'])
							: '',
						ucfirst($event),
						($eventExists)
							? ' ' . $item['Stadtname']
							: ''
					 ));
		}
		$html .= '
							</p>
						</div>
					</div>
				</div>';
		echo $html;
	}
	echo"
			</div>
			<!-- SINGPOINT LANDKARTE ENDE -->\n";
?>